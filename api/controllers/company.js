const mongoose = require('mongoose');

const Company = require('../models/company');

const vacationRules = require('../models/vacationRules');

exports.companies_get_all = (request, response, next) => {
  Company.find()
    .populate('vacationRule', '_id name')
    .populate('defaultRule')
    .exec()
    .then(docs => {
      console.log(docs);
      const res = {
        count: docs.length,
        companies: docs.map( doc => {
          return doc
        })
      }
      if (docs.length >= 0) {
        response.status(200).json(res);
      } else {
        response.status(404).json({
          message: 'No entries found'
        });
      }
    })
    .catch(error => {
      console.log(error);
      response.status(500).json({error: error});
    })
};

exports.companies_create_company = (request, response, next) => { //single -. one file only
  const req = new Company({
    _id: new mongoose.Types.ObjectId(),
    name: request.body.name,
    email: request.body.email,
    emailBase: request.body.emailBase,
    phone: request.body.phone,
    language: request.body.language,
    address: request.body.address,
    country: request.body.country,
    vacationRule: request.body.vacationRule,
    defaultRule: request.body.defaultRule
  });
  req.save().then(result => {
    console.log(result);
    response.status(201).json ({
      message: 'Created company successfully',
      company: result,
      request: {
        type: 'POST',
        url: 'http://localhost:3000/rule/' + result._id
      }
    });
  }).catch(error => {
    console.log(error);
    response.status(500).json({error: error});
  });
};

exports.companies_get_company = (request, response, next) => {
  const companyId = request.params.companyId;
  Company.findById(companyId)
  .populate('vacationRule', '_id name')
  .populate('defaultRule')
  .exec()
  .then(doc => {
    console.log("From database: ", doc);
    if (doc) {
      response.status(200).json({
        company: doc,
        request: {
          type: 'GET',
          url: 'http://localhost:3000/company/' + doc._id
        }
      });
    } else {
      response.status(404).json({
        message: 'No valid entry found for provided ID'
      });
    }
  })
  .catch(error => {
    console.log(error);
    response.status(500).json({error: error});
  });
};


exports.companies_update_company = (request, response, next) => {
  const id = request.params.companyId;
  const updateOperations = {};
  for (const ops of Object.keys(request.body)) {
    updateOperations[ops] = request.body[ops];
  }
  Company.updateOne({_id:id}, { $set: updateOperations })
    .populate('vacationRule', '_id name')
    .populate('defaultRule')
    .exec()
    .then(result => {
      console.log(result);
      response.status(200).json({
        message: 'Company updated',
        request: {
          type: 'GET',
          url: 'http://localhost:3000/company/' + id
        }
      });
    })
    .catch(error => {
      console.log(error);
      response.status(500).json({error: error});
    });
};

exports.companies_delete_company = (request, response, next) => {
  const id = request.params.companyId
  Company.deleteOne({_id:id})
    .exec()
    .then(result => {
      response.status(200).json({
        message: 'Company deleted',
        result: result
      });
    })
    .catch(error => {
      console.log(error);
      response.status(500).json({
        error: error
      });
    });
};