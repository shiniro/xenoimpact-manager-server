const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const Request = require('../models/requests');
const User = require('../models/user');


exports.requests_get_all = (request, response, next) => {
  Request.find()
    .populate('user')
    .exec()
    .then(docs => {
      console.log(docs);
      const res = {
        count: docs.length,
        requests: docs.map( doc => {
          return doc
        })
      }
      if (docs.length >= 0) {
        response.status(200).json(res);
      } else {
        response.status(404).json({
          message: 'No entries found'
        });
      }
    })
    .catch(error => {
      console.log(error);
      response.status(500).json({error: error});
    })
};


exports.requests_get_all_by_company = (request, response, next) => {
  const companyId = request.params.companyId;
  Request.find().populate({
      path: 'user',
      match: {
        company: companyId
      }
    })
    .exec()
    .then(docs => {
      console.log(docs);
      const res = {
        count: docs.length,
        requests: docs.map( doc => {
          return doc
        })
      }
      if (docs.length >= 0) {
        response.status(200).json(res);
      } else {
        response.status(404).json({
          message: 'No entries found'
        });
      }
    })
    .catch(error => {
      console.log(error);
      response.status(500).json({error: error});
    })
};


exports.requests_create_request = (request, response, next) => { //single -. one file only
  console.log(request.file);
  console.log(request.body.fileUrl);
  const req = new Request({
    _id: new mongoose.Types.ObjectId(),
    nbDays: request.body.nbDays,
    startDate: request.body.startDate,
    endDate: request.body.endDate,
    type: request.body.type,
    status: request.body.status,
    fileUrl: request.file !== undefined ? request.file.path : null,
    user: request.body.user,
    comment: request.body.comment
  });
  req.save().then(result => {
    console.log(result);

    return User.findById(request.body.user)
    .exec()
    .then(user => {
      if (user) {
        const updateOperations = {};

        if(request.body.type == 'day off') {
          updateOperations.daysOff = user.daysOff - request.body.nbDays;
          updateOperations.daysOffTaken = user.daysOffTaken + request.body.nbDays;
        } else if (request.body.type == 'summer vacation') {
          updateOperations.summerVacation = user.summerVacation - request.body.nbDays;
          updateOperations.summerVacationTaken = user.summerVacationTaken + request.body.nbDays;
        } else if (request.body.type == 'medical leave') {
          updateOperations.medicalLeaveTaken = user.medicalLeaveTaken + request.body.nbDays;
        } else {
          updateOperations.daysOffNoPaidTaken = user.daysOffNoPaidTaken + request.body.nbDays;
        }

        return User.updateOne({_id:request.body.user}, { $set: updateOperations })
          .exec()
          .then(result => {
            console.log(result);
            response.status(201).json ({
              message: 'Created request successfully',
              createdRequest: result,
              request: {
                type: 'POST',
                url: 'http://localhost:3000/requests/' + result._id
              }
            });
          })
          .catch(error => {
            console.log(error);
            response.status(500).json({error: error});
          });
      } else {
        return response.status(404).json({
          message: 'No valid entry found for provided ID'
        });
      }
    })
    .catch(error => {
      console.log(error);
      response.status(500).json({error: error});
    });
    
  }).catch(error => {
    console.log(error);
    response.status(500).json({error: error});
  });
};

exports.requests_get_request = (request, response, next) => {
  const requestId = request.params.requestId;
  Request.findById(requestId)
  .populate('user', 'name')
  .exec()
  .then(doc => {
    console.log("From database: ", doc);
    if (doc) {
      response.status(200).json({
        result: doc,
        request: {
          type: 'GET',
          url: 'http://localhost:3000/requests/' + doc._id
        }
      });
    } else {
      response.status(404).json({
        message: 'No valid entry found for provided ID'
      });
    }
  })
  .catch(error => {
    console.log(error);
    response.status(500).json({error: error});
  });
};


exports.requests_get_request_by_userId = (request, response, next) => {
  const userId = request.params.userId;
  Request.find({'user': userId})
  .populate('user', 'name')
  .exec()
  .then(doc => {
    console.log("From database: ", doc);
    if (doc) {
      response.status(200).json({
        result: doc,
        request: {
          type: 'GET',
          url: 'http://localhost:3000/requests/user/' + userId
        }
      });
    } else {
      response.status(404).json({
        message: 'No valid entry found for provided ID'
      });
    }
  })
  .catch(error => {
    console.log(error);
    response.status(500).json({error: error});
  });
};


exports.requests_update_request = (request, response, next) => {
  const id = request.params.requestId;
  const updateOperations = {};
  for (const ops of Object.keys(request.body)) {
    updateOperations[ops] = request.body[ops];
  }
  Request.updateOne({_id:id}, { $set: updateOperations })
    .exec()
    .then(result => {
      console.log(result);
      response.status(200).json({
        message: 'Request updated',
        request: {
          type: 'GET',
          url: 'http://localhost:3000/requests/' + id
        }
      });
    })
    .catch(error => {
      console.log(error);
      response.status(500).json({error: error});
    });
};


exports.requests_delete_request = (request, response, next) => {
  const id = request.params.requestId
  Request.deleteOne({_id:id})
    .exec()
    .then(result => {
      response.status(200).json({
        message: 'Request deleted',
        result: result
      });
    })
    .catch(error => {
      console.log(error);
      response.status(500).json({
        error: error
      });
    });
};