const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const User = require('../models/user');
const Company = require('../models/company');
const Request = require('../models/requests');

let today = new Date();

exports.users_get_all = (request, response, next) => {
  User.find()
  .populate('company')
  .populate('vacationRule')
    .exec()
    .then(docs => {
      console.log(docs);
      const res = {
        count: docs.length,
        users: docs.map( doc => {
          return doc
        })
      }
      if (docs.length >= 0) {
        response.status(200).json(res);
      } else {
        response.status(404).json({
          message: 'No entries found'
        });
      }
    })
    .catch(error => {
      console.log(error);
      response.status(500).json({error: error});
    })
};

exports.user_signup = (request, response, next) => {
  User.find({ email: request.body.email })
    .exec()
    .then(user => {
      if (user.length >= 1) {
        return response.status(409).json({
          message: 'Email already used'
        }) // 409 = conflict - 422 = unproccessable entity
      } else {
        bcrypt.hash(request.body.password, 10, (error, hash) => {
          if (error) {
            return response.status(500).json({
              error: error
            });
          } else {
            return Company.findOne({'emailBase': request.body.email.split('@')[1]})
            .exec()
            .then(company => {
              const user = new User({
                _id: new mongoose.Types.ObjectId(),
                email: request.body.email,
                name: request.body.name,
                password: hash,
                pictureUrl: request.body.pictureUrl,
                company: company ? company._id : null,
                vacationRule: company ? company.defaultRule : null,
                os: request.body.os,
                pushId: request.body.pushId,
                token: request.body.token,
                admin: false,
                superAdmin: false,
                daysOff: today.getMonth() + 1,
                summerVacation: 4,
                summerVacationTaken: 0,
                daysOffNoPaidTaken: 0,
                daysOffTaken: 0,
                medicalLeaveTaken: 0,
                nbDaysOffPerYear: null,
                nbSummerVacationPerYear: null,
                language: request.body.language,
                autoActionOnChangeYear: true,
                resetOnChangeYear: true
              });
              
              user.save()
              .then(result => {
                console.log(result);
                response.status(201).json({
                  message: 'User created',
                  user: result
                });
              })
              .catch(error => {
                console.log(error);
                response.status(500).json({
                  error: error
                });
              });
            })
            .catch(error => {
              console.log(error);
              response.status(500).json({error: error});
            });
          }
        }); // 10 random number for more security
      }
    })
};

exports.user_login = (request, response, next) => {
  User.findOne({ email: request.body.email }) // can use findOne to get just one user
    .populate('company')
    .populate('vacationRule')
    .exec()
    .then(user => {
      if (user.length < 1) {
        return response.status(401).json({
          messages: 'Authentification failed'
        }); // 401 => got no email or password wrong - authentification failed
      } else {
        bcrypt.compare(request.body.password, user.password, (error, result) => {
          if (error) {
            return response.status(401).json({
              messages: 'Authentification failed - email incorrect'
            });
          } 
          if (result) {
            const token = jwt.sign(
              {
                email: user.email,
                userId: user._id
              }, 
              "julieXenoimpact1218",
              {
                expiresIn: "1h"
              }
            );

            const updateOperations = {
              'token': token,
              'lastLogin': today.getTime(),
              'expiryTime': today.getTime() + (60*60*1000)
            };

            if(user.lastLogin != null && today.getFullYear() != new Date(user.lastLogin).getFullYear() && user.autoActionOnChangeYear == 'true') {
              updateOperations.summerVacation = (today.getMonth() + 1) >= 3 ? 4 : (user.resetOnChangeYear == 'true' ? 0 : user.summerVacation + 4),
              updateOperations.daysOff = user.resetOnChangeYear == 'true' ? today.getMonth() + 1 : user.daysOff + today.getMonth() + 1
              updateOperations.daysOffTaken = 0,
              updateOperations.summerVacationTaken = 0,
              updateOperations.daysOffNoPaidTaken = 0
              updateOperations.medicalLeaveTaken = 0
            } else if (user.lastLogin != null && today.getMonth() > new Date(user.lastLogin).getMonth()) {
              updateOperations.daysOff = user.daysOff + 1
            }

            return User.updateOne({_id:user._id}, { $set: updateOperations })
              .exec()
              .then(result => {
                return response.status(201).json({
                  messages: 'Authentification successful',
                  token: token,
                  user: user
                });
              })
              .catch(error => {
                console.log(error);
                return response.status(500).json({error: error});
              });
          }
          return response.status(401).json({
            messages: 'Authentification failed - password incorrect'
          });
        });
      }
    })
    .catch( error => {
      console.log(error);
      response.status(500).json({
        error: error
      });
    });
};


exports.users_get_user = (request, response, next) => {
  const userId = request.params.userId;
  User.findById(userId)
  .populate('company')
  .populate('vacationRule')
  .exec()
  .then(doc => {
    console.log("From database: ", doc);
    if (doc) {
      return response.status(200).json({
        user: doc,
        request: {
          type: 'GET',
          url: 'http://localhost:3000/user/' + doc._id
        }
      });
    } else {
      return response.status(404).json({
        message: 'No valid entry found for provided ID'
      });
    }
  })
  .catch(error => {
    console.log(error);
    response.status(500).json({error: error});
  });
};


exports.users_get_user_by_email = (request, response, next) => {
  const userEmail = request.params.userEmail;
  console.log(userEmail)
  User.findOne({'email': userEmail})
  .populate('company')
  .populate('vacationRule')
  .exec()
  .then(doc => {
    console.log("From database: ", doc);
    if (doc) {
      response.status(200).json({
        user: doc
      });
    } else {
      response.status(404).json({
        message: 'No valid entry found for provided ID'
      });
    }
  })
  .catch(error => {
    console.log(error);
    response.status(500).json({error: error});
  });
};



exports.users_get_users_by_companyId = (request, response, next) => {
  const companyId = request.params.companyId;
  User.find({'company': companyId})
  .populate('company')
  .populate('vacationRule')
  .exec()
  .then(doc => {
    console.log("From database: ", doc);
    if (doc) {
      response.status(200).json({
        count: doc.length,
        users: doc
      });
    } else {
      response.status(404).json({
        message: 'No valid entry found for provided ID'
      });
    }
  })
  .catch(error => {
    console.log(error);
    response.status(500).json({error: error});
  });
};


exports.users_update_user = (request, response, next) => {
  const id = request.params.userId;
  const updateOperations = {};
  console.log(request.body)
  for (const ops of Object.keys(request.body)) {
    updateOperations[ops] = request.body[ops];
  }
  console.log('update')
  console.log(updateOperations)
  User.updateOne({_id:id}, { $set: updateOperations })
    .populate('company')
    .populate('vacationRule')
    .exec()
    .then(result => {
      console.log(result);
      response.status(200).json({
        message: 'User updated',
        request: {
          type: 'PUT',
          url: 'http://localhost:3000/user/' + id
        }
      });
    })
    .catch(error => {
      console.log(error);
      response.status(500).json({error: error});
    });
};


exports.user_delete_user = (request, response, next) => {
  User.deleteOne({ _id: request.params.userId })
    .exec()
    .then(result => {
      return Request.deleteMany({ user: request.params.userId })
        .exec()
        .then(result => {
          response.status(200).json({
            message: "User deleted"
          })
        }).catch(error => {
          console.log(error);
          response.status(500).json({
            error: error
          });
        });;
    })
    .catch(error => {
      console.log(error);
      response.status(500).json({
        error: error
      });
    });
};