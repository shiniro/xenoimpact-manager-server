const mongoose = require('mongoose');

const VacationRule = require('../models/vacationRules');


exports.rules_get_all = (request, response, next) => {
  VacationRule.find()
    .exec()
    .then(docs => {
      console.log(docs);
      const res = {
        count: docs.length,
        rules: docs.map( doc => {
          return doc
        })
      }
      if (docs.length >= 0) {
        response.status(200).json(res);
      } else {
        response.status(404).json({
          message: 'No entries found'
        });
      }
    })
    .catch(error => {
      console.log(error);
      response.status(500).json({error: error});
    })
};

exports.rules_create_rule = (request, response, next) => {
  const req = new VacationRule({
    _id: new mongoose.Types.ObjectId(),
    name: request.body.name,
    autoActionOnChangeYear: request.body.autoActionOnChangeYear,
    resetOnChangeYear: request.body.resetOnChangeYear,
    nbDaysOffPerYear: request.body.nbDaysOffPerYear,
    nbSummerVacationPerYear: request.body.nbSummerVacationPerYear,
    resetDate: request.body.resetDate,
    addEachMonth: request.body.addEachMonth,
    nbDayAddEachMonth: request.body.nbDayAddEachMonth,
    summerVacationAddDate: request.body.summerVacationAddDate
  });
  req.save().then(result => {
    console.log(result);
    response.status(201).json ({
      message: 'Created rule successfully',
      rule: result,
      request: {
        type: 'POST',
        url: 'http://localhost:3000/rule/' + result._id
      }
    });
  }).catch(error => {
    console.log(error);
    response.status(500).json({error: error});
  });
};

exports.rules_get_rule = (request, response, next) => {
  const ruleId = request.params.ruleId;
  VacationRule.findById(ruleId)
  .exec()
  .then(doc => {
    console.log("From database: ", doc);
    if (doc) {
      response.status(200).json({
        rule: doc,
        request: {
          type: 'GET',
          url: 'http://localhost:3000/rule/' + doc._id
        }
      });
    } else {
      response.status(404).json({
        message: 'No valid entry found for provided ID'
      });
    }
  })
  .catch(error => {
    console.log(error);
    response.status(500).json({error: error});
  });
};


exports.rules_update_rule = (request, response, next) => {
  const id = request.params.ruleId;
  const updateOperations = {};
  for (const ops of Object.keys(request.body)) {
    updateOperations[ops] = request.body[ops];
  }
  VacationRule.updateOne({_id:id}, { $set: updateOperations })
    .exec()
    .then(result => {
      console.log(result);
      response.status(200).json({
        message: 'Rule updated',
        request: {
          type: 'GET',
          url: 'http://localhost:3000/rule/' + id
        }
      });
    })
    .catch(error => {
      console.log(error);
      response.status(500).json({error: error});
    });
};

exports.rules_delete_rule = (request, response, next) => {
  const id = request.params.ruleId
  VacationRule.deleteOne({_id:id})
    .exec()
    .then(result => {
      response.status(200).json({
        message: 'Rule deleted',
        result: result
      });
    })
    .catch(error => {
      console.log(error);
      response.status(500).json({
        error: error
      });
    });
};