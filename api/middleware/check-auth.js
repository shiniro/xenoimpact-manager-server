const jwt = require('jsonwebtoken');

module.exports = (request, response, next) => {
    try {
        const token = request.headers.authorization.split(" ")[1];
        const decoded = jwt.verify(token, "julieXenoimpact1218");
        request.userData = decoded;
        next();
    } catch (error) {
        return response.status(401).json({
            message: 'Authentification failed'
        });
    }
};