const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const companySchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  name: { type: String, required: true },
  email: { type: String, required: true },
  emailBase: { type: String, required: true },
  phone: { type: String, required: true },
  language: { type: String, required: true },
  address: { type: String, required: true },
  country: { type: String, required: true },
  vacationRules: [{ type: Schema.Types.ObjectId, ref: 'VacationRule' }],
  defaultRule: { type: Schema.Types.ObjectId, ref: 'VacationRule' }
},
{ timestamps: {
    createdAt: 'created_at',
    updatedAt: 'update_at'
  }
});

module.exports = mongoose.model('Company', companySchema);