const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const requestSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  nbDays: { type: Number, required: true },
  startDate: {
    type: String,
    required: true,
    match: /^\d{4}[\/](0?[1-9]|1[012])[\/](0?[1-9]|[12][0-9]|3[01])$/
  },
  endDate: {
    type: String,
    required: true,
    match: /^\d{4}[\/](0?[1-9]|1[012])[\/](0?[1-9]|[12][0-9]|3[01])$/
  },
  type: { 
    type: String,
    required: true,
    validate: {
      validator: function(value) {
        return /day off|medical leave|summer vacation|no paid/i.test(value);
      },
      message: props => `${props.value} is not a valid type.`
    }
  },
  status: {
    type: String,
    required: true,
    validate: {
      validator: function(value) {
        return /pending|approved|rejected/i.test(value);
      },
      message: props => `${props.value} is not a valid status.`
    }
  },
  fileUrl: { type: String },
  user: { type: Schema.Types.ObjectId, ref: 'User' },
  comment: { type: String }
},
{ timestamps: {
    createdAt: 'created_at',
    updatedAt: 'update_at'
  }
});

module.exports = mongoose.model('Request', requestSchema);