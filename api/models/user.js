const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  email: {
    type: String,
    required: true,
    // unique: true,
    match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
  },
  name: { type: String },
  password: { type: String },
  pictureUrl: {
    type: String,
    match: /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/
  },
  dateOfBirth: { 
    type: String,
    match: /^\d{4}[\/](0?[1-9]|1[012])[\/](0?[1-9]|[12][0-9]|3[01])$/
  },
  company: { type: Schema.Types.ObjectId, ref: 'Company' },
  vacationRule: { type: Schema.Types.ObjectId, ref: 'VacationRule' },
  os: { type: String, required: true },
  pushId: { type: String, required: true },
  token: { type: String },
  admin: { type: Boolean, required: true },
  superAdmin: { type: Boolean, required: true },
  daysOff: { type: Number, required: true },
  summerVacation: { type: Number, required: true },
  summerVacationTaken: { type: Number, required: true },
  daysOffTaken: { type: Number, required: true },
  daysOffNoPaidTaken: { type: Number, required: true },
  medicalLeaveTaken: { type: Number, required: true },
  language: { type: String },
  nbDaysOffPerYear: { type: Number },
  nbSummerVacationPerYear: { type: Number },
  autoActionOnChangeYear: { type: Boolean, required: true },
  resetOnChangeYear: { type: Boolean, required: true },
  lastLogin: { type: Number },
  expiryTime: { type: Number },
},
{ timestamps: {
  createdAt: 'created_at',
  updatedAt: 'update_at'
}
});

module.exports = mongoose.model('User', userSchema);