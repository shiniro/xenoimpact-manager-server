const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const vacationRuleSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  name: { type: String, required: true },
  autoActionOnChangeYear: { type: Boolean, required: true },
  resetOnChangeYear: { type: Boolean, required: true },
  nbDaysOffPerYear: { type: Number },
  nbSummerVacationPerYear: { type: Number },
  resetDate: { type: String },
  addEachMonth: { type: Boolean },
  nbDayAddEachMonth: { type: Number },
  summerVacationAddDate: { type: String }
},
{ timestamps: {
    createdAt: 'created_at',
    updatedAt: 'update_at'
  }
});

module.exports = mongoose.model('VacationRule', vacationRuleSchema);