const express = require("express");
const router = express.Router();

const CompanyController = require('../controllers/company');
const checkAuth = require('../middleware/check-auth');

router.get('/', checkAuth, CompanyController.companies_get_all);

router.post("/", checkAuth, CompanyController.companies_create_company);

router.get('/:companyId', checkAuth, CompanyController.companies_get_company);

router.put('/:companyId', checkAuth, CompanyController.companies_update_company);

router.delete("/:companyId", checkAuth, CompanyController.companies_delete_company);

module.exports = router;
