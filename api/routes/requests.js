const express = require("express");
const router = express.Router();
const multer = require('multer');

const RequestController = require('../controllers/requests');
const checkAuth = require('../middleware/check-auth');

const storage = multer.diskStorage({
  destination: function (request, file, callback) {
    callback(null, './uploads/');
  },
  filename: function (request, file, callback) {
    callback(null, file.originalname); // callback(null, newDate().toISOString() + file.originalname)
  }
});


const fileFilter = (request, file, callback) => {
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' || file.mimetype === 'image/gif' || file.mimetype === 'image/bmp' || file.mimetype === 'image/jpg') {
    callback(null, true); // accept file
  } else {
    callback(null, false); // reject file
  }
};

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5 // 5Mbits
  },
  //fileFilter: fileFilter
}); //specified a folder multer will store files (not publicly accessible -> staic folder)


router.get('/', checkAuth, RequestController.requests_get_all);
router.get('/company/:companyId', checkAuth, RequestController.requests_get_all_by_company);

router.post("/", checkAuth, upload.single('fileUrl'), RequestController.requests_create_request);

router.get('/:requestId', checkAuth, RequestController.requests_get_request);

router.get('/user/:userId', checkAuth, RequestController.requests_get_request_by_userId);

router.put('/:requestId', checkAuth, upload.single('fileUrl'), RequestController.requests_update_request);

router.delete("/:requestId", checkAuth, RequestController.requests_delete_request);

module.exports = router;
