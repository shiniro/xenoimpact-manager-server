const express = require("express");
const router = express.Router();

const UserController = require('../controllers/user');
const checkAuth = require('../middleware/check-auth');

router.get('/', checkAuth, UserController.users_get_all);
router.get('/company/:companyId', UserController.users_get_users_by_companyId);

router.post("/signup", UserController.user_signup);

router.post("/login", UserController.user_login);

router.get('/:userId', checkAuth, UserController.users_get_user);
router.get('/email/:userEmail', UserController.users_get_user_by_email);

router.put('/:userId', checkAuth, UserController.users_update_user);

router.delete("/:userId", checkAuth, UserController.user_delete_user);

module.exports = router;
