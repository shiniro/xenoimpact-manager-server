const express = require("express");
const router = express.Router();

const VacationRulesController = require('../controllers/vacationRules');
const checkAuth = require('../middleware/check-auth');

router.get('/', checkAuth, VacationRulesController.rules_get_all);

router.post("/", checkAuth, VacationRulesController.rules_create_rule);

router.get('/:ruleId', checkAuth, VacationRulesController.rules_get_rule);

router.put('/:ruleId', checkAuth, VacationRulesController.rules_update_rule);

router.delete("/:ruleId", checkAuth, VacationRulesController.rules_delete_rule);

module.exports = router;
