const express = require("express");
const app = express();
const morgan = require("morgan");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const userRoutes = require('./api/routes/user');
const requestsRoutes = require('./api/routes/requests');
const companyRoutes = require('./api/routes/company');
const rulesRoutes = require('./api/routes/vacationRules');

mongoose.connect(
  'mongodb+srv://julieXenoimpact1218:julie_xenoimpact@xenoimpactmanagerserver-kvl75.mongodb.net/test?retryWrites=true&w=majority',
  { useNewUrlParser: true }
);
mongoose.Promise = global.Promise;

app.use(morgan("dev"));
app.use('/uploads', express.static('uploads'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use((request, response, next) => {
  response.header("Access-Control-Allow-Origin", "*");
  response.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  if (request.method === "OPTIONS") {
    response.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
    return response.status(200).json({});
  }
  next();
});

// Routes which should handle requests
app.use("/rules", rulesRoutes);
app.use("/company", companyRoutes);
app.use("/user", userRoutes);
app.use("/requests", requestsRoutes);

app.use((request, response, next) => {
  const error = new Error("Not found");
  error.status = 404;
  next(error);
});

app.use((error, request, response, next) => {
  response.status(error.status || 500);
  response.json({
    error: {
      message: error.message
    }
  });
});

module.exports = app;
